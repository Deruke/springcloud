package com.example.demo.enums;

import lombok.Getter;

/**
 * Created by 千叶星城 on 2018/10/28.
 */
@Getter
public enum PayStatusEnum {

    WAIT(0,"等待支付"),
    SUCCESS(1,"支付成功"),
    ;
    private Integer code;
    private String message;
    PayStatusEnum(Integer code ,String message){
        this.code = code ;
        this.message = message;
    }





}
