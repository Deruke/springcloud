package com.example.demo.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by 千叶星城 on 2018/10/28.
 */
// 设置服务端名称
@FeignClient(name = "product")
public interface ProductClinet {


    @GetMapping("/msg")
    String productMsg();
}
