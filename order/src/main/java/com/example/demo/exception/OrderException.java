package com.example.demo.exception;

import com.example.demo.enums.ResultEnum;

/**
 * Created by 千叶星城 on 2018/10/28.
 */
public class OrderException extends RuntimeException{

    private Integer code;

    public OrderException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public OrderException(ResultEnum resultEnum) {
        super(resultEnum.getMessage());
        this.code = resultEnum.getCode();
    }
}
