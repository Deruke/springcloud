package com.example.demo.service;

import com.example.demo.dto.OrderDTO;

/**
 * Created by 千叶星城 on 2018/10/28.
 */
public interface OrderService {

    /**
     * 创建订单
     * @param orderDTO
     * @return
     */
    OrderDTO create(OrderDTO orderDTO);
}
