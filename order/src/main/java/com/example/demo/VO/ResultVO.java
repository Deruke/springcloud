package com.example.demo.VO;

import lombok.Data;

/**
 * Created by 千叶星城 on 2018/10/28.
 */
@Data
public class ResultVO<T> {
    private Integer code;

    private String msg;

    private T data;
}
