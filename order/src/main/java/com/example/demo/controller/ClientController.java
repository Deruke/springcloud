package com.example.demo.controller;

import com.example.demo.client.ProductClinet;
import com.sun.deploy.nativesandbox.comm.Response;
import com.sun.media.jfxmedia.logging.Logger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

/**
 * Created by 千叶星城 on 2018/10/28.
 */
@RestController
@Slf4j
public class ClientController {

    @Autowired
    private LoadBalancerClient loadBalancerClient;


//    @Autowired
//    private RestTemplate restTemplate;

    @GetMapping("/getProductMsg")
    public String getProductMsg(){
        // 1、第一种方式
        // ip地址不明确 分布式多个服务不确定性
        RestTemplate restTemplate = new RestTemplate();
        String response = restTemplate.getForObject("http://localhost:8080/msg",String.class);
        log.info("response={}", response);
        return response;
    }
    // 第二种方式
    @GetMapping("/getProductMsg2")
    public String getProductMsg2() {
        RestTemplate restTemplate = new RestTemplate();
        ServiceInstance serviceInstance = loadBalancerClient.choose("PRODUCT");
        String url = String.format("http://%s:%s", serviceInstance.getHost(), serviceInstance.getPort());
        String response = restTemplate.getForObject(url, String.class);

        log.info("response={}", response);
        return response;
    }

    // 第三种方式
    // 1、利用LoadBe
//    @GetMapping("/getProductMsg3")
//    public String getProductMsg3(){
//
//        String Response = restTemplate.getForObject("http://PRODUCT/msg",String.class);
//        log.info("response={}", Response);
//        return Response;
//    }

    // Feign的使用
    @Autowired
    private ProductClinet productClinet;

    @GetMapping("/getProductMsg4")
    public String getProductMsg4(){
        String response = productClinet.productMsg();
        log.info("response={}",response);
        return response;
    }

}
