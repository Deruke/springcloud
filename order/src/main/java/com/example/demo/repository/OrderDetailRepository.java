package com.example.demo.repository;

import com.example.demo.dataobject.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by 千叶星城 on 2018/10/28.
 */
public interface OrderDetailRepository extends JpaRepository<OrderDetail,String> {

}
