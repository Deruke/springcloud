package com.example.demo.utils;

import com.example.demo.VO.ResultVO;

/**
 * Created by 千叶星城 on 2018/10/28.
 */
public class ResultVOUtil {
    public static ResultVO success(Object object) {
        ResultVO resultVO = new ResultVO();
        resultVO.setCode(0);
        resultVO.setMsg("成功");
        resultVO.setData(object);
        return resultVO;
    }
}
