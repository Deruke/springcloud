package com.example.demo.utils;

import java.util.Random;

/**
 * Created by 千叶星城 on 2018/10/28.
 */
public class KeyUtil {
    /**
     * 生成唯一主键
     * 格式：时间+随机数
     */
    public static synchronized String genUniqueKey(){
        Random random = new Random();
        Integer number = random.nextInt(900000)+10000;
    return System.currentTimeMillis()+ String.valueOf(number);
    }
}
