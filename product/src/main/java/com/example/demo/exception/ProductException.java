package com.example.demo.exception;

/**
 * Created by 千叶星城 on 2018/10/27.
 */
public class ProductException extends RuntimeException{

    private Integer code;

    public ProductException(Integer code, String message) {
        super(message);
        this.code = code;
    }

//    public ProductException(ResultEnum resultEnum) {
//        super(resultEnum.getMessage());
//        this.code = resultEnum.getCode();
//    }
}
