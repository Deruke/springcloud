package com.example.demo.service.impl;

import com.example.demo.dataobject.ProductInfo;
import com.example.demo.enums.ProductStatusEnum;
import com.example.demo.repository.ProductInfoRepository;
import com.example.demo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by 千叶星城 on 2018/10/28.
 */
@Service
public class ProductServiceImpl implements ProductService{
    @Autowired
    private ProductInfoRepository productInfoRepository;

    @Override
    public List<ProductInfo> findUpAll() {
        return productInfoRepository.findByProductStatus(ProductStatusEnum.DOWN.getCode());
    }
}
