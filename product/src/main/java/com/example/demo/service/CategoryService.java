package com.example.demo.service;

import com.example.demo.dataobject.ProductCategory;

import java.util.List;

/**
 * Created by 千叶星城 on 2018/10/28.
 */
public interface CategoryService {
    List<ProductCategory> findByCategoryTypeIn(List<Integer> categoryTypeList);
}
