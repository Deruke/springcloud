package com.example.demo.service;

import com.example.demo.dataobject.ProductInfo;

import java.util.List;

/**
 * Created by 千叶星城 on 2018/10/27.
 */
public interface ProductService {
    /**
     * 查询所有在架商品列表
     */
    List<ProductInfo> findUpAll();

    /**
     * 查询商品列表
     * @param productIdList
     * @return
     */
    // List<ProductInfoOutput> findList(List<String> productIdList);

    /**
     * 扣库存
     * @param decreaseStockInputList
     */
    // void decreaseStock(List<DecreaseStockInput> decreaseStockInputList);



}
