package com.example.demo.VO;

import lombok.Data;

/**
 * Created by 千叶星城 on 2018/10/27.
 */
@Data
public class ResultVO<T> {
    /**
     * 错误码
     */
    private Integer code;

    /**
     * 提示信息
     */
    private String msg;

    /**
     * 具体内容
     */
    private T data;
}
