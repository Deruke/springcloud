package com.example.demo.repository;

import com.example.demo.dataobject.ProductInfo;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

/**
 * Created by 千叶星城 on 2018/10/28.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductRepositoryTest {
    @Autowired
    private ProductInfoRepository productInfoRepository;

    @Test
    public void findByProductStatus() throws Exception{
        List<ProductInfo> list = productInfoRepository.findByProductStatus(1);
        Assert.assertTrue(list.size()>0);
    }

    @Test
    public void findByProductIdIn() throws Exception{

        List<ProductInfo> list = productInfoRepository.findByProductIdIn(Arrays.asList("123456","123457"));
        System.out.println(list.size());
    }









}
