package com.example.demo.service;

import com.example.demo.ProductApplicationTests;
import com.example.demo.dataobject.ProductInfo;
import junit.framework.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by 千叶星城 on 2018/10/28.
 */
@Component
public class ProductServiceTest extends ProductApplicationTests {

    @Autowired
    private ProductService productService;

    @Test
    public void findUpAll() throws Exception {
        List<ProductInfo> list = productService.findUpAll();
        Assert.assertTrue(list.size() > 0);
    }

//    @Test
//    public void findList() throws Exception {
//        List<ProductInfoOutput> list = productService.findList(Arrays.asList("157875196366160022", "157875227953464068"));
//        Assert.assertTrue(list.size() > 0);
//    }

//    @Test
//    public void decreaseStock() throws Exception {
//        DecreaseStockInput decreaseStockInput = new DecreaseStockInput("157875196366160022", 2);
//        productService.decreaseStock(Arrays.asList(decreaseStockInput));
//    }
}
